/**
 * This class will contain the main program in which it will solve the given sudoku
 * in the fashion of 2D int arrays
 * 
 * 
 */
public class Solver
{
    public static void main(String [] args)
    {
        //Testing checkOneRow method
        int r1[] = {1,2,3,4,5,6,7,8,9};
        System.out.println(checkOneRow(r1));
        int r2[] = {2,2,3,4,5,6,7,8,9};
        System.out.println(checkOneRow(r2));
        int r3[] = {5,2,3,4,1,6,7,8,9};
        System.out.println(checkOneRow(r3));
        int r4[] = {5,2,4,3,9,6,7,8,1};
        System.out.println(checkOneRow(r4));
        int r5[] = {999,2,4,3,9,6,7,8,1};
        System.out.println(checkOneRow(r5));
        
        int s1[][] = {{8,2,7,1,5,4,3,9,6},
                      {9,6,5,3,2,7,1,4,8},
                      {3,4,1,6,8,9,7,5,2},
                      {5,9,3,4,6,8,2,7,1},
                      {4,7,2,5,1,3,6,8,9},
                      {6,1,8,9,7,2,4,3,5},
                      {7,8,6,2,3,5,9,1,4},
                      {1,5,4,7,9,6,8,2,3},
                      {2,3,9,8,4,1,5,6,7}};
        
        System.out.println(checkAllRow(s1));
        System.out.println(checkAllCol(s1));
        System.out.println(checkSubSquares(s1)+" true!");
        
        int sq1[][] = {{3,9,6},
                   {1,4,8},
                   {7,5,2}};
        System.out.println(checkOneSquare(sq1));
        
        int sb1[][] = {{8,2,7,1,5,4,3,9,6},
                      {9,6,5,3,2,7,1,4,8},
                      {3,4,1,6,8,9,7,5,2}};
        System.out.println(checkRowSubSquare(sb1)+" :(");
        
    }
    
    //The solver method
    public static 
    
    //The main method of checking validity that combines everything
    public static boolean isValid(int board[][])
    {
        if(checkAllCol(board) && checkAllRow(board) && checkSubSquares(board))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public static boolean checkOneRow(int oneRow[])
    {
        boolean comparing[] = new boolean[9];
        
        for(int i=0;i<oneRow.length;i++)
        {
            //We are checking the validity of the elements
            if(oneRow[i] <= 0 || oneRow[i] >= 10)
            {
                return false;
            }
            
            //This means that the number is not yet marked. So we mark it
            if(!comparing[oneRow[i]-1])
            {
                comparing[oneRow[i]-1] = true;
            }
            //If the previous if-statement is false then that means one of the number
            //repeated thus we have to return false meaning that the row is not valid
            else
            {
                return false;
            }
        }

        //Now if we outside of the for loop that means a number did not repeat
        //hence all of the numbers are present or else it will return false inside the
        //for loop hence return true
        return true;
    }
    
    //These three algorithms will be used to perform checks to determine whether 
    //or not the Sudoku is valid
    public static boolean checkAllRow(int board[][])
    {
        for(int r=0;r<9;r++)
        {
            //We only return false if one of the rows is not valid, if none of the
            //row are false then that means the all of the rows are valid
            if(!checkOneRow(board[r]))
            {
                return false;
            }
        }
        
        //If we are outside then that means all of the row are valid hence we return true
        return true;
    }
    
    //This algorithm will be checking whether all the columns are valid
    public static boolean checkAllCol(int board[][])
    {
        //We are going to column first order to check the validity of all columns
        for(int c=0;c<9;c++)
        {
            //This array will be collecting all of the column array elements for one column and will be
            //feed into checkOneRow to check the validity of each column
            int oneRow[] = new int[9];
            
            for(int r=0;r<9;r++)
            {
                oneRow[r] = board[c][r];
            }
            
            //This means that one of the column of the board is invalid hence we return false
            if(!checkOneRow(oneRow))
            {
                return false;
            }
        }
        
        //If we are outside that means all of the columns are valid hence we return true
        return true;
    }
    
    //This algorithm will be checking if one of the 3x3 square contains 1-9 value ithout
    //repetition
    public static boolean checkOneSquare(int subSquare[][])
    {
        boolean check[] = new boolean[9];
        
        for(int r=0;r<3;r++)
        {
            for(int c=0;c<3;c++)
            {
                int element = subSquare[r][c];
                
                //Checking the validity of the element4
                if(element <= 0 || element >= 10)
                {
                    return false;
                }
                
                if(!check[element-1])
                {
                    check[element-1] = true;
                }
                //Repeated elements
                else
                {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    //This algorithm will be cheking if all of the 3z3 square in the board are valid
    //we will be calling checkOneSquare inside checkSubSquare
    public static boolean checkSubSquares(int board[][])
    {
        for(int r=0;r<9;r=r+3)
        {
            int rowSquares[][] = new int[3][9];
            rowSquares[0] = board[r];
            rowSquares[1] = board[r+1];
            rowSquares[2] = board[r+2];
            
            if(!checkRowSubSquare(rowSquares))
            {
                return false;
            }
        }
        
        return true;
    }
    
    public static boolean checkRowSubSquare(int squareRow[][])
    {
        for(int c=0;c<9;c=c+3)
        {
            int square[][] = new int[3][3];
            int sr = 0;
            int sc =0;
            for(int r=0;r<3;r++)
            {
                square[sr][sc] = squareRow[r][c];
                square[sr][sc+1] = squareRow[r][c+1];
                square[sr][sc+2] = squareRow[r][c+2];
                
                sr++;
                sc = 0;
            }
            
            if(!checkOneSquare(square))
            {
                return false;
            }
        }
        
        return true;
    }
    
    public static void printBoard(int board[][])
    {
        for(int r=0;r<9;r++)
        {
            for(int c=0;c<9;c++)
            {
                System.out.print(board[r][c]+" ");
            }
            
            System.out.println("");
        }
    }
}